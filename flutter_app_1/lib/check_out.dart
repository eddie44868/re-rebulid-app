import 'package:flutter/material.dart';
import 'package:flutter_app_1/widgets/order_summary.dart';

class CheckOut extends StatelessWidget {
  const CheckOut({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double widthScreen = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        automaticallyImplyLeading: false,
        title: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  InkWell(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Container(
                      height: 45,
                      width: 45,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        color: Colors.white
                      ),
                      child: Center(
                        child: Image.asset("assets/back.png", height: 20, width: 20,),
                      ),
                    ),
                  ),
                  Text("Check Out", style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold, color: Colors.black)),
                  Container(
                      height: 45,
                      width: 45,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        color: Colors.white
                      ),
                    ),
                ],
              ),
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: SafeArea(
          child: Padding(
            padding: EdgeInsets.only(left: 15, right: 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 10,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("Shipping Address", style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.black)),
                    Container(
                      height: 30,
                      width: 70,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Colors.green[100]
                      ),
                      child: Center(
                        child: Text("Edit", style: TextStyle(fontSize: 15, fontWeight: FontWeight.w400, color: Colors.green)),
                      ),
                    )
                  ],
                ),
                SizedBox(height: 10,),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      height: 40,
                      width: 40,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Colors.green[100]
                      ),
                      child: Center(
                        child: Icon(Icons.location_on_outlined, color: Colors.green, size: 25,),
                      ),
                    ),
                    SizedBox(width: 20,),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("David M. Cowherd\n+1-202-555***4", style: TextStyle(fontSize: 15, fontWeight: FontWeight.w300, color: Colors.grey)),
                        SizedBox(height: 10),
                        Text("20 Ridgefield Terrace, Shelton,ct,\n6484 United States - 6484", style: TextStyle(fontSize: 15, fontWeight: FontWeight.w300, color: Colors.grey)),
                      ],
                    ),
                  ],
                ),
                SizedBox(height: 20),
                Text("Order Summary", style: TextStyle(fontSize: 15, fontWeight: FontWeight.w500, color: Colors.black)),
                SizedBox(height: 10),
                OrderSummary(imageUrl: "assets/nanas.png", text1: "Pineapple", text2: "Fruit", text3: "1"),
                OrderSummary(imageUrl: "assets/semangka.png", text1: "Watermelon", text2: "Fruit", text3: "1"),
                OrderSummary(imageUrl: "assets/papaya.png", text1: "Papaya", text2: "Fruit", text3: "1"),
                OrderSummary(imageUrl: "assets/kubis.png", text1: "Cabbage", text2: "Vegetables", text3: "2"),
                SizedBox(height: 10),
                Container(
                  height: 40,
                  width: widthScreen-20,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.grey[100]
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Center(
                            child: Icon(Icons.price_change_outlined, color: Colors.green, size: 25,),
                          ),
                          SizedBox(width: 20),
                          Center(
                            child: Text("Apply promo code", style: TextStyle(fontSize: 15, fontWeight: FontWeight.w300, color: Colors.grey))),
                        ],
                      ),
                      Container(
                        height: 30,
                        width: 60,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Colors.green
                        ),
                        child: Center(
                          child: Text("Apply", style: TextStyle(fontSize: 15, fontWeight: FontWeight.w400, color: Colors.white)),
                        ),
                      )
                    ],
                  )
                ),
              ],
            ),
          )
        ),
      ),
      bottomNavigationBar: Padding(
        padding: EdgeInsets.all(10),
        child: SizedBox(
          width: widthScreen-30,
          height: 40,
          child: TextButton(
            style: TextButton.styleFrom(
              backgroundColor: Colors.green,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10)
              )
            ),
            onPressed: () {
              //Navigator.push(context, MaterialPageRoute(builder: (context) => CheckOut()));
            },
            child: Text(
              "Go to Payment",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 15,
                  fontWeight: FontWeight.bold),
            ),
          ),
        ),
      )
    );
  }
}