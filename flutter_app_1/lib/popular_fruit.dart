import 'package:flutter/material.dart';
import 'package:flutter_app_1/shopping_cart.dart';
import 'package:flutter_app_1/widgets/product.dart';
import 'package:readmore/readmore.dart';

// ignore: must_be_immutable
class PopularFruit extends StatefulWidget {
  String imageUrl;
  String name;
  PopularFruit({ Key? key, required this.imageUrl, required this.name }) : super(key: key);

  @override
  _PopularFruitState createState() => _PopularFruitState();
}

class _PopularFruitState extends State<PopularFruit> {
  int _counter = 0;

  void _plus() {
    setState(() {
      _counter++;
    });
  }
  void _minus() {
    setState(() {
      _counter--;
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.orange[50],
        automaticallyImplyLeading: false,
        title: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  InkWell(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Container(
                      height: 45,
                      width: 45,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        color: Colors.white
                      ),
                      child: Center(
                        child: Image.asset("assets/back.png", height: 20, width: 20,),
                      ),
                    ),
                  ),
                  Container(
                    height: 45,
                    width: 45,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      color: Colors.white
                    ),
                    child: Center(
                      child: Icon(Icons.favorite, size: 25, color: Colors.red,),
                    ),
                  ),
                ],
              ),
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Column(
          children: [
            Stack(
              children: [
                Container(
                  alignment: Alignment.topCenter,
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  child: Container(
                    height: 350.0,
                    decoration: BoxDecoration(
                      color: Colors.orange[50],
                      borderRadius: BorderRadius.vertical(
                          bottom: Radius.elliptical(
                              MediaQuery.of(context).size.width, 350.0)),
                    ),
                  ),
                ),
                SafeArea(
                  child: Padding(
                    padding: EdgeInsets.only(left: 10, right: 10, top: 10),
                    child: Column(
                        children: [
                          SizedBox(height: 5,),
                          Image.asset(widget.imageUrl, height: 200, width: 200,),
                          SizedBox(height: 15,),
                          Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Container(
                                  height: 6,
                                  width: 10,
                                  decoration: BoxDecoration(
                                    color: Colors.grey[200],
                                    borderRadius: BorderRadius.circular(15)
                                  ),
                                ),
                                SizedBox(width: 5,),
                                Container(
                                  height: 6,
                                  width: 15,
                                  decoration: BoxDecoration(
                                    color: Colors.green[200],
                                    borderRadius: BorderRadius.circular(15)
                                  ),
                                ),
                                SizedBox(width: 5,),
                                Container(
                                  height: 6,
                                  width: 10,
                                  decoration: BoxDecoration(
                                    color: Colors.grey[200],
                                    borderRadius: BorderRadius.circular(15)
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(height: 10),
                            Container(
                              height: 30,
                              width: 100,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.white
                              ),
                              child: Center(
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                      children: [
                                        Text("\$6.70", style: TextStyle(fontSize: 15, fontWeight: FontWeight.w400, color: Colors.green)),
                                        Text("/kg", style: TextStyle(fontSize: 12, fontWeight: FontWeight.w600)),
                                      ],
                                    )
                                ),
                            ),
                            SizedBox(height: 15),
                            Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text("Fresh Natural\n${widget.name}", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                          InkWell(
                                            onTap: _minus,
                                            child: Container(
                                              height: 30,
                                              width: 30,
                                              decoration: BoxDecoration(
                                                borderRadius: BorderRadius.circular(5),
                                                color: Colors.green
                                              ),
                                              child: Center(
                                                child: Image.asset("assets/minus.png", color: Colors.white, height: 20, width: 15,),
                                              )
                                            ),
                                          ),
                                          SizedBox(width: 15),
                                          Text("$_counter Kg", style: TextStyle(fontSize: 13, fontWeight: FontWeight.bold)),
                                          SizedBox(width: 15),
                                          InkWell(
                                            onTap: _plus,
                                            child: Container(
                                              height: 30,
                                              width: 30,
                                              decoration: BoxDecoration(
                                                borderRadius: BorderRadius.circular(5),
                                                color: Colors.green
                                              ),
                                              child: Center(
                                                child: Icon(Icons.add, size: 20, color: Colors.white,),
                                              )
                                            ),
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                  SizedBox(height: 5),
                                  Row(
                                    children: [
                                      Image.asset("assets/star.png", height: 40, width: 120,),
                                      SizedBox(width: 5),
                                      Text("(5.0)", style: TextStyle(fontSize: 17, fontWeight: FontWeight.normal, color: Colors.grey[400]),)
                                    ],
                                  ),
                                  SizedBox(height: 10),
                                  Text("Description", style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),),
                                  SizedBox(height: 10),
                                  ReadMoreText(
                                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua",
                                    trimLines: 2,
                                    colorClickableText: Colors.green,
                                    trimMode: TrimMode.Line,
                                    trimCollapsedText: 'Read more',
                                    trimExpandedText: 'Read less',
                                    style: TextStyle(fontSize: 13, fontWeight: FontWeight.w400, color: Colors.black54),
                                  ),
                                  SizedBox(height: 20),
                                  Text("Similiar Product", style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),),
                                  SizedBox(height: 15),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Product(imageUrl: "assets/nanas.png", text: "Pineapple"),
                                      Product(imageUrl: "assets/semangka.png", text: "Watermelon"),
                                      Product(imageUrl: "assets/papaya.png", text: "Papaya"),
                                      Product(imageUrl: "assets/straw.png", text: "Strawberry"),
                                    ],
                                  ),
                                  SizedBox(height: 15),
                                  Product(imageUrl: "assets/straw.png", text: "Strawberry"),
                                  SizedBox(height: 15),
                                ],
                              ),
                        ],
                      ),
                  )
                ),
              ],
            ),
          ],
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Padding(
                padding: EdgeInsets.only(left: 10),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Text(
                        "Total",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 15,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        "\$13.4",
                        style: TextStyle(
                            color: Colors.green,
                            fontSize: 20,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
              label: "",
            ),
            BottomNavigationBarItem(
              icon: SizedBox(
                width: 160,
                height: 40,
                child: TextButton(
                  style: TextButton.styleFrom(
                    backgroundColor: Colors.green,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)
                    )
                  ),
                  onPressed: () {
                    Navigator.push(context, MaterialPageRoute(builder: (context) => ShoppingCart()));
                  },
                  child: Text(
                    "Add Cart",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 15,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              label: "",
            )
          ]),
      );
  }
}