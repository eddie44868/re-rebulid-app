import 'package:flutter/material.dart';
import 'package:flutter_app_1/bottom_nav.dart';

class StartUp extends StatelessWidget {
  const StartUp({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double widthScreen = MediaQuery.of(context).size.width;
    final double heightScreen = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: Colors.green[100],
      body: Column(
        children: [
          Stack(
            children: [
              SafeArea(
                child: Image.asset("assets/girl.png")
                ),
              Column(
                children: [
                  SizedBox(height: heightScreen-420),
                  Container(
                    height: 420,
                    width: widthScreen,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.vertical(
                          top: Radius.elliptical(
                              MediaQuery.of(context).size.width, 150.0)),
                    ),
                    child: Column(
                      children: [
                        SizedBox(height: 100,),
                        Text("Let's find the best &\n  healthy Grocery", style: TextStyle(fontSize: 30, fontWeight: FontWeight.w500)),
                        SizedBox(height: 20,),
                        Text("Online food delivery - Quick and \neasy to find grocery", style: TextStyle(fontSize: 15, fontWeight: FontWeight.w300), textAlign: TextAlign.center,),
                        Center(),
                        SizedBox(height: 50,),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                              height: 6,
                              width: 10,
                              decoration: BoxDecoration(
                                color: Colors.grey[200],
                                borderRadius: BorderRadius.circular(15)
                              ),
                            ),
                            SizedBox(width: 5,),
                            Container(
                              height: 6,
                              width: 10,
                              decoration: BoxDecoration(
                                color: Colors.grey[200],
                                borderRadius: BorderRadius.circular(15)
                              ),
                            ),
                            SizedBox(width: 5,),
                            Container(
                              height: 6,
                              width: 15,
                              decoration: BoxDecoration(
                                color: Colors.green[200],
                                borderRadius: BorderRadius.circular(15)
                              ),
                            )
                          ],
                        ),
                        SizedBox(height: 30,),
                        InkWell(
                          onTap: () {
                            Navigator.push(context, MaterialPageRoute(builder: (context) => BottomNav()));
                          },
                          child: Container(
                          height: 60,
                          width: widthScreen-60,
                          decoration: BoxDecoration(
                            color: Colors.green,
                            borderRadius: BorderRadius.circular(15)
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text("Get Started", style: TextStyle(fontSize: 15, fontWeight: FontWeight.w500, color: Colors.white))
                            ],
                          ),
                        ),
                        ),
                      ],
                    ),
                  )
                ],
              )
            ],
          ),
        ],
      ),
    );
  }
}