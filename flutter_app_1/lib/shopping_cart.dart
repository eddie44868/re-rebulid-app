import 'package:flutter/material.dart';
import 'package:flutter_app_1/check_out.dart';
import 'package:flutter_app_1/widgets/shopping_bar.dart';

class ShoppingCart extends StatelessWidget {
  const ShoppingCart({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        automaticallyImplyLeading: false,
        title: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  InkWell(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Container(
                      height: 45,
                      width: 45,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        color: Colors.white
                      ),
                      child: Center(
                        child: Image.asset("assets/back.png", height: 20, width: 20,),
                      ),
                    ),
                  ),
                  Text("Shopping Cart", style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold, color: Colors.black)),
                  Container(
                      height: 45,
                      width: 45,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        color: Colors.white
                      ),
                    ),
                ],
              ),
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: SafeArea(
          child: Padding(
            padding: EdgeInsets.only(left: 10, right: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ShoppingBar(imageUrl: "assets/straw.png", text1: 'Strawberry', text2: "1"),
                ShoppingBar(imageUrl: "assets/jeruk.png", text1: 'Orange', text2: "1"),
                ShoppingBar(imageUrl: "assets/Apple.png", text1: 'Apple', text2: "1"),
                ShoppingBar(imageUrl: "assets/kubis.png", text1: 'Cabbage', text2: "2"),
              ],
            ),
          )
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Padding(
                padding: EdgeInsets.only(left: 10),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Text(
                        "Total",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 15,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        "\$26.1",
                        style: TextStyle(
                            color: Colors.green,
                            fontSize: 20,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
              label: "",
            ),
            BottomNavigationBarItem(
              icon: SizedBox(
                width: 160,
                height: 40,
                child: TextButton(
                  style: TextButton.styleFrom(
                    backgroundColor: Colors.green,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)
                    )
                  ),
                  onPressed: () {
                    Navigator.push(context, MaterialPageRoute(builder: (context) => CheckOut()));
                  },
                  child: Text(
                    "Checkout",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 15,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              label: "",
            )
          ]),
    );
  }
}