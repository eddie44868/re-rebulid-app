import 'package:flutter/material.dart';
import 'package:flutter_app_1/popular_fruit.dart';
import 'package:flutter_app_1/widgets/bar_top.dart';
import 'package:flutter_app_1/widgets/categories_bar.dart';
import 'package:flutter_app_1/widgets/popular_bar.dart';

class Home extends StatelessWidget {
  const Home({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        backgroundColor: Colors.white,
        automaticallyImplyLeading: false,
        title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Image.asset("assets/category.png", height: 25, width: 25,),
              Image.asset("assets/notification.png", height: 30, width: 30,),
            ],
          ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.only(right: 15, left: 15, top: 10),
          child: SafeArea(
              child: Column(
                children: [
                  SizedBox(height: 10,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        height: 40,
                        width: 250,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(15),
                          color: Colors.grey[200]
                        ),
                        child: Row(
                          children: [
                            SizedBox(width: 10,),
                            Icon(
                              Icons.search, 
                              size: 25,
                            ),
                            SizedBox(width: 10,),
                            Text("Search", style: TextStyle(fontSize: 15, fontWeight: FontWeight.w200),)
                          ],
                        ),
                      ),
                      Container(
                        height: 60,
                        width: 60,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(15),
                          color: Colors.green
                        ),
                        child: Center(
                          child: Image.asset("assets/settings.png", color: Colors.white, height: 25, width: 25,),
                        )
                      ),
                    ],
                  ),
                  SizedBox(height: 20,),
                  SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Row(
                      children: [
                        BarTop(image: "assets/bar1.png", colors: Color(0xffa5d6a7), text1: "Up to 30% offer!", text2: "Enjoy our big offer of \nevery day", theme1: Color(0xff4caf50), theme2: Color(0xff4caf50),),
                        BarTop(image: "assets/bar1.png", colors: Color(0xff90caf9), text1: "Up to 30% offer!", text2: "Enjoy our big offer of \nevery day", theme1: Color(0xff2196f3), theme2: Color(0xff2196f3),),
                      ],
                    ),
                  ),
                  SizedBox(height: 20),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("Categories", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
                      Text("See all", style: TextStyle(fontSize: 15, fontWeight: FontWeight.w400, color: Colors.green)),
                    ],
                  ),
                  SizedBox(height: 20),
                  SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Row(
                      children: [
                        CategoriesBar(text: "Vegetables", imageUrl: 'assets/organic.png',),
                        CategoriesBar(text: "Fruits", imageUrl: 'assets/fruit.png',),
                        CategoriesBar(text: "Milk & Eggs", imageUrl: 'assets/food.png',),
                        CategoriesBar(text: "Drinks", imageUrl: 'assets/drink.png',),
                        CategoriesBar(text: "Flours", imageUrl: 'assets/flour.png',),
                      ],
                    )
                  ),
                  SizedBox(height: 25),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("Popular", style: TextStyle(fontSize: 19, fontWeight: FontWeight.bold)),
                      Text("See all", style: TextStyle(fontSize: 15, fontWeight: FontWeight.w400, color: Colors.green)),
                    ],
                  ),
                  SizedBox(height: 20),
                  Padding(
                    padding: EdgeInsets.only(left: 10, right: 10, bottom: 10),
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            InkWell(
                              onTap: () {
                                Navigator.push(context, MaterialPageRoute(builder: (context) => PopularFruit(imageUrl: 'assets/Apple.png', name: 'Apple',)));
                              },
                              child: PopularBar(imageUrl: "assets/Apple.png", text: "Apple"),
                            ),
                            InkWell(
                              onTap: () {
                                Navigator.push(context, MaterialPageRoute(builder: (context) => PopularFruit(imageUrl: 'assets/semangka.png', name: '',)));
                              },
                              child: PopularBar(imageUrl: "assets/semangka.png", text: "Watermelon"),
                            ),
                          ],
                        ),
                        SizedBox(height: 25),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            InkWell(
                              onTap: () {
                                Navigator.push(context, MaterialPageRoute(builder: (context) => PopularFruit(imageUrl: 'assets/nanas.png', name: 'Pineapple',)));
                              },
                              child: PopularBar(imageUrl: "assets/nanas.png", text: "Pineapple"),
                            ),
                            InkWell(
                              onTap: () {
                                Navigator.push(context, MaterialPageRoute(builder: (context) => PopularFruit(imageUrl: 'assets/papaya.png', name: 'Papaya',)));
                              },
                              child: PopularBar(imageUrl: "assets/papaya.png", text: "Papaya"),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ],
              )
            ),
          )
        )
    );
  }
}