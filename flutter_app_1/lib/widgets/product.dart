import 'package:flutter/material.dart';

// ignore: must_be_immutable
class Product extends StatelessWidget {
  String imageUrl;
  String text;

  Product({ Key? key, required this.imageUrl, required this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
          children: [
            Container(
              height: 70,
              width: 70,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.green[100]
              ),
              child: Center(
                child: Image.asset(imageUrl, height: 40, width: 40,),
              )
            ),
            SizedBox(height: 10),
            Text(text, style: TextStyle(fontSize: 12, fontWeight: FontWeight.normal)),
          ],
        );
  }
}