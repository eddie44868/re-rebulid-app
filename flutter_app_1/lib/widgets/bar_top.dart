import 'package:flutter/material.dart';

// ignore: must_be_immutable
class BarTop extends StatelessWidget {
  //
  Color colors;
  Color theme1;
  Color theme2;
  String image;
  String text1;
  String text2;
  BarTop({ Key? key, required this.colors, required this.image, required this.text1, required this.text2, required this.theme1, required this.theme2 }) : super(key: key);
  //Colors.green[100]
  //"assets/bar1.png"
  //"Up to 30% offer!"
  //"Enjoy our big offer of \nevery day"

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(
            height: 125,
            width: 300,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
              color: colors
            ),
            child: Row(
              children: [
                Center(
                  child: Image.asset(image, height: 130, width: 130,),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(text1, style: TextStyle(fontSize: 20, color: theme1)),
                        SizedBox(height: 10,),
                        Text(text2, style: TextStyle(fontSize: 15,)),
                        SizedBox(height: 10,),
                        Container(
                          height: 30,
                          width: 80,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            color: theme2
                          ),
                          child: Center(
                            child: Text("Order Site", style: TextStyle(fontSize: 10, color: Colors.white)),
                          )
                        ),
                      ],
                    ),
                  ),
              ],
            ),
          ),
          SizedBox(width: 10,)
      ],
    );
  }
}