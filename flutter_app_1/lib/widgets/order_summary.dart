import 'package:flutter/material.dart';

// ignore: must_be_immutable
class OrderSummary extends StatelessWidget {
  String imageUrl;
  String text1;
  String text2;
  String text3;
  OrderSummary({ Key? key, required this.imageUrl, required this.text1, required this.text2, required this.text3}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                Container(
                  height: 70,
                  width: 70,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.green[100]
                  ),
                  child: Center(
                    child: Image.asset(imageUrl, height: 40, width: 40,),
                  )
                ),
                SizedBox(width: 20,),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(text1, style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.black)),
                    Text(text2, style: TextStyle(fontSize: 15, fontWeight: FontWeight.w400, color: Colors.grey)),
                    Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text("\$6.70", style: TextStyle(fontSize: 12, fontWeight: FontWeight.w400, color: Colors.green)),
                        Text("/kg", style: TextStyle(fontSize: 10, fontWeight: FontWeight.w600)),
                      ],
                    )
                  ],
                )
              ],
            ),
            Text("X $text3", style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),)
          ],
        ),
        SizedBox(height: 15,)
      ],
    );
  }
}