import 'package:flutter/material.dart';

// ignore: must_be_immutable
class ShoppingBar extends StatefulWidget {
  String imageUrl;
  String text1;
  String text2;
  ShoppingBar({ Key? key, required this.imageUrl, required this.text1, required this.text2}) : super(key: key);

  @override
  _ShoppingBarState createState() => _ShoppingBarState();
}

class _ShoppingBarState extends State<ShoppingBar> {
  int _counter = 0;

  void _plus() {
    setState(() {
      _counter++;
    });
  }
  void _minus() {
    setState(() {
      _counter--;
    });
  }
  @override
  Widget build(BuildContext context) {
    final double widthScreen = MediaQuery.of(context).size.width;
    return Column(
      children: [
        SizedBox(height: 10),
        Container(
          height: 85,
          width: widthScreen-20,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: Colors.grey[200]
          ),
          child: Column(
            children: [
              SizedBox(height: 10),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      SizedBox(width: 10),
                      Center(
                          child: Image.asset(widget.imageUrl, height: 60, width: 60,),
                      ),
                      SizedBox(width: 20,),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(widget.text1, style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold, color: Colors.black)),
                          Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text("\$6.70", style: TextStyle(fontSize: 15, fontWeight: FontWeight.w400, color: Colors.green)),
                              Text("/kg", style: TextStyle(fontSize: 12, fontWeight: FontWeight.w600)),
                            ],
                          )
                        ],
                      )
                    ],
                  ),
                  Padding(
                    padding: EdgeInsets.only(right: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        InkWell(
                          onTap: _minus,
                          child: Container(
                            height: 20,
                            width: 20,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              color: Colors.green
                            ),
                            child: Center(
                              child: Image.asset("assets/minus.png", color: Colors.white, height: 15, width: 10,),
                            )
                          ),
                        ),
                        SizedBox(width: 15),
                        Text("$_counter Kg", style: TextStyle(fontSize: 13, fontWeight: FontWeight.bold)),
                        SizedBox(width: 15),
                        InkWell(
                          onTap: _plus,
                          child: Container(
                            height: 20,
                            width: 20,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              color: Colors.green
                            ),
                            child: Center(
                              child: Icon(Icons.add, size: 15, color: Colors.white,),
                            )
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              SizedBox(height: 15,)
            ],
          ),
        ),
      ],
    );
  }
}