import 'package:flutter/material.dart';

// ignore: must_be_immutable
class PopularBar extends StatelessWidget {
  String imageUrl;
  String text;
  PopularBar({ Key? key, required this.imageUrl,  required this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
            height: 180,
            width: 140,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: Colors.grey[100]
            ),
            child: Column(
                children: [
                  Padding(
                    padding: EdgeInsets.only(top: 5, right: 5),
                    child: Align(
                        alignment: Alignment.topRight,
                        child: Icon(Icons.favorite, color: Colors.red,),
                      ),
                    ),
                  Image.asset(imageUrl, height: 85, width: 85,),
                  SizedBox(height: 10),
                  Container(
                    height: 56,
                    width: 140,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.green[100]
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Padding(
                          padding: EdgeInsets.only(top: 5, left: 5),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(text, style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold)),
                                SizedBox(height: 5),
                                Row(
                                  children: [
                                    Text("\$6.70", style: TextStyle(fontSize: 15, fontWeight: FontWeight.w400, color: Colors.green)),
                                    Text("/kg", style: TextStyle(fontSize: 10, fontWeight: FontWeight.w400)),
                                  ],
                                )
                              ],
                            ),
                          ),
                          Container(
                            height: 40,
                            width: 40,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(15),
                                bottomLeft: Radius.circular(15),
                              ),
                              color: Colors.green
                            ),
                            child: Center(
                              child: Icon(Icons.add, size: 20, color: Colors.white,),
                            ),
                          )
                      ],
                    ),
                  )
                ],
              )
              );
  }
}